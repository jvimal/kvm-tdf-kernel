#!/bin/bash

export CONCURRENCY_LEVEL=9

fakeroot make-kpkg --initrd --append-to-version -tdf kernel_image
fakeroot make-kpkg --append-to-version -tdf kernel_headers

